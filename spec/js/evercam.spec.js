describe("Evercam", function () {

  $(document).ajaxError(function (event, request, settings) {
    console.log("Error requesting page " + settings.url);
  });

  var subject = Evercam;

  describe("#apiUrl", function () {

    it("defaults to the evercam api via https", function () {
      expect(subject.apiUrl).toBe('https://dashboard.evercam.io/v1');
    });

    it("can be overridden to another url", function () {
      subject.setApiUrl('http://proxy.evercam.io:3000/v1');
      expect(subject.apiUrl).toBe('http://proxy.evercam.io:3000/v1');
      subject.setApiUrl('http://localhost:9292/v1');
    });

  });

  describe("Models", function () {

    it("get all", function (done) {
      subject.Model.all()
      .then(function (vendors) {
        expect(vendors.length).toBeGreaterThan(0);
        done();
      });
    });

    it("get by vendor", function (done) {
      subject.Model.by_vendor('other')
      .then(function (vendor) {
        expect(vendor.id).toBe('other');
        done();
      });
    });

    it("get by model", function (done) {
      subject.Model.by_model('other', 'Other')
      .then(function (model) {
        expect(model.vendor).toBe('other');
        expect(model.name).toBe('Other');
        done();
      });
    });

  });

  describe("Cameras", function () {

    it("create", function (done) {
      subject.setBasicAuth('joeyb', '12345');
      subject.Camera.create({'id': 'testcamera', 'endpoints': ['http://127.0.0.1:8080'],
          'is_public': true, 'name': 'Test Camera', "snapshots": {'jpg': '/onvif/snapshot'},
          'auth': {'basic': {'username': 'user1', 'password': 'abcde'}}})
      .then(function (camera) {
        expect('testcamera').toBe(camera.id);
        expect(true).toBe(camera.is_public);
        done();
      });
    });

    it('get by id', function (done) {
      subject.Camera.by_id('testcamera')
      .then(function (camera) {
        expect(camera).not.toBe(undefined);
        done();
      });
    });

  });

  describe("Users", function () {

    it("create", function (done) {
      subject.User.create({forename: 'Joe', lastname: 'Bloggs', email: 'joe.bloggs@example.org',
          username: 'joeyb', country: 'us'})
      .then(function (user) {
          expect('joeyb').toBe(user.id);
          expect('Joe').toBe(user.forename);
          done();
        });
    });

    it("cameras", function (done) {
      subject.User.cameras('joeyb')
      .then(function (cameras) {
        expect(cameras.length).toBe(2);
        done();
      });
    });

  });

  describe("Vendors", function () {

    it("all", function (done) {
      subject.Vendor.all()
      .then(function (vendors) {
        expect(vendors.length).toBe(2);
        done();
      });
    });

    it("by mac", function (done) {
      subject.Vendor.by_mac('00:73:57')
      .then(function (vendors) {
        expect(vendors.length).toBe(1);
        done();
      });
    });

  });

});

